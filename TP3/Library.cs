﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    public class Library
    {
        /// <summary>
        /// Insert a new Game into the library
        /// </summary>
        /// <param name="allGames">The current library</param>
        /// <param name="gameInfo">The new Game to insert</param>
        /// <exception cref="ArgumentException">If the parameters are invalid</exception>
        public static void InsertGame(List<Game> allGames, Game gameInfo)
        {
            if(gameInfo is null)
            {
                throw new ArgumentNullException($"The parameter {nameof(gameInfo)} cannot be null");
            }
            allGames.Add(gameInfo);
        }

        /// <summary>
        /// Remove a game from the library
        /// </summary>
        /// <param name="allGames">The library</param>
        /// <param name="index">Index of the game to remove</param>
        /// <exception cref="ArgumentException">If the parameters are invalid</exception>
        public static void RemoveGame(List<Game> allGames, int index)
        {
            if(index < 0)
            {
                throw new ArgumentException($"The index cannot be a negative value");
            }

            if(allGames.Count < index)
            {
                throw new ArgumentException($"The index {index} is greater than the size of the library");
            }

            allGames.RemoveAt(index);
        }

        /// <summary>
        /// Update the game at the index in the library with the new informations
        /// </summary>
        /// <param name="allGames">The library</param>
        /// <param name="index">The index the game to update is at</param>
        /// <param name="newGame">The new informations</param>
        /// <exception cref="ArgumentException">If the parameters are invalid</exception>
        public static void UpdateGame(List<Game> allGames, int index, Game newGame)
        {
            if (allGames.Count < index)
            {
                throw new ArgumentException($"The index {index} is greater than the size of the library");
            }

            if (index < 0)
            {
                throw new ArgumentException($"The index cannot be a negative value");
            }

            if (newGame is null)
            {
                throw new ArgumentException($"The parameter {nameof(newGame)} cannot be null");
            }

            allGames[index] = newGame;
        }

        /// <summary>
        /// Get the string to display the content of the library with
        /// </summary>
        /// <param name="games">List of the games in the library</param>
        /// <param name="publishers">List of the tpublishers available</param>
        /// <returns>The string to display the content of the library</returns>
        public static string GetLibraryString(List<Game> games,
                                              List<Publisher> publishers)
        {
            var stringBuilder = new StringBuilder();
            var longestGameTitleLength = GetLongestGameTitleLength(games);
            var defaultTitleLength = longestGameTitleLength + 10;

            stringBuilder.AppendLine($"      Title{new string(' ', defaultTitleLength)}Year    Category    Rating  Publisher");
            stringBuilder.AppendLine($"      ====={new string(' ', defaultTitleLength)}====    ========    ======  =========");

            for (int i = 0; i < games.Count; i++)
            {
                var game = games[i];
                var publisher = publishers[game.GamePublisher];
                var titleToYearBlanckCharachter = new string(' ', (defaultTitleLength - game.Title.Length) + 5); // The 5 account for the length of the column name (Title)
                var categoryToRatingBlanckCharacter = new string(' ', 8 - game.GameCategory.Length); // The 8 account for the length of the column name (Category)
                stringBuilder.AppendLine($"{i}-    {game.Title}{titleToYearBlanckCharachter}{game.Year}    {game.GameCategory}{categoryToRatingBlanckCharacter}       {game.Rating}     {publisher.Name}");
            }

            return stringBuilder.ToString();
        }

        private static int GetLongestGameTitleLength(List<Game> games)
        {
            var longestTitleLength = 0;

            foreach ( var game in games)
            {
                if(game.Title.Length > longestTitleLength)
                {
                    longestTitleLength = game.Title.Length;
                }
            }

            return longestTitleLength;
        }
    }
}
