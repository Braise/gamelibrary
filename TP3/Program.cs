﻿namespace TP3
{
    public class Program
    {
        // Sources des données
        public const string PUBLISHERS_FILE = "publishers.csv";
        public const string GAMES_FILE = "games.csv";
        public const char ACTOR_SEPARATION_TOKEN = ',';

        // Identification des colonnes 
        public const int COLUMN_TITLE = 0;
        public const int COLUMN_YEAR = 1;
        public const int COLUMN_CATEGORY = 2;
        public const int COLUMN_RATING = 3;
        public const int COLUMN_PUBLISHER = 4;
        public const int COLUMN_COUNT = COLUMN_PUBLISHER + 1;

        // Types de jeux


        // List statiques

        public static List<Game> gamesLibrary = new List<Game>();
        public static List<Publisher> publisherReposetory = new List<Publisher>();

        public static void Main(string[] args)
        {
            try
            {
                var publishers = FileHandler.GetPublishers(PUBLISHERS_FILE, ACTOR_SEPARATION_TOKEN);
                var games = FileHandler.GetGames(GAMES_FILE, ACTOR_SEPARATION_TOKEN);
                UserInterface.Run(publishers, games);
            }
            catch(Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" An unexpected error occured. Please restart.");
                Console.ResetColor();
                Console.ReadLine();
            }
        }
    }
}