﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    public class Game
    {
        public const int RTS = 0;
        public const int SHOOTER = 1;
        public const int MOBA = 2;
        public const int RPG = 3;
        public const int SANDBOX = 4;
        public static int[] CATEGORIES = { RTS, SHOOTER, MOBA, RPG, SANDBOX };
        public static string[] CATEGORIES_NAMES = { "RTS", "SHOOTER", "MOBA", "RPG", "SANDBOX" };

        public string Title { get; set; }

        public int Year { get; set; }

        public string GameCategory { get; set; }

        public int Rating { get; set; }

        public int GamePublisher { get; set; }

        public static Game CreateGame(string title,
                                      int year,
                                      int gameCategory,
                                      int rating,
                                      int gamePublisher)
        {
            return new Game()
            {
                Title = title,
                Year = year,
                GameCategory = CATEGORIES_NAMES[gameCategory],
                Rating = rating,
                GamePublisher = gamePublisher
            };
        }

        public static string GetAvailableCategoriesString()
        {
            string toDisplay = ("Available categories : ");
            foreach(int i in CATEGORIES)
            {
                toDisplay += $"{i}-{CATEGORIES_NAMES[i]}    ";
            }

            return toDisplay;
        }

        public static bool IsGameTitleValid(string gameTitle)
        {
            return !string.IsNullOrWhiteSpace(gameTitle);
        }

        public static int GetCategoryFromName(string categoryName)
        {
            for (int i = 0; i < CATEGORIES_NAMES.Length; i++)
            {
                if (CATEGORIES_NAMES[i] == categoryName)
                {
                    return i;
                }
            }

            throw new ArgumentException($"The category {categoryName} is unknown");
        }
    }
}
