﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
  public class Publisher
  {
        // Créer la structur ici avec les informations pertinentes:
        // Name et Country (string)
        public string Name { get; set; }
        public string Country { get; set; }

        public static Publisher CreatePublisher(string name,
                                                string country)
        {
            return new Publisher()
            {
                Name = name,
                Country = country
            };
        }

        /// <summary>
        /// Display the content of the list of the available publishers
        /// </summary>
        /// <param name="publishers">The available publishers</param>
        public static string GetAvailablePublishersString(List<Publisher> publishers)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Available Publishers");

            for (int i = 0; i < publishers.Count; i++)
            {
                stringBuilder.AppendLine($"{i} - {publishers[i].Name}");
            }
            
            return stringBuilder.ToString();
        }
    }
}
