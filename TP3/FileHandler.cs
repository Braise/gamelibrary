﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    /// <summary>
    /// Interact with the file system to get and save informations
    /// </summary>
    public static class FileHandler
    {
        /// <summary>
        /// Get all the publishers in the file.
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        /// <param name="separator">The separator to split a line upon</param>
        /// <returns>The publishers</returns>
        /// <exception cref="InvalidDataException">If the file does not exists</exception>
        /// <exception cref="FileNotFoundException">If the file is empty or a line does not respect the agreed upon format</exception>
        public static List<Publisher> GetPublishers(string filePath, 
                                                    char separator)
        {
            if (File.Exists(filePath))
            {
                var lines = File.ReadAllLines(filePath);

                if(lines.Length == 0)
                {
                    throw new InvalidDataException($"The file {filePath} is empty");
                }

                var publishers = new List<Publisher>();

                foreach(var line in lines)
                {
                    var publisherInformations = line.Split(separator);

                    if(publisherInformations.Length != 2)
                    {
                        throw new InvalidDataException($"The line : {line} in the file {filePath} does not respect the expected format <Name, Country>");
                    }

                    var publisher = Publisher.CreatePublisher(publisherInformations[0],
                                                              publisherInformations[1]);
                    publishers.Add(publisher);
                }

                return publishers;
            }

            throw new FileNotFoundException($"The file path {filePath} is invalid");
        }

        /// <summary>
        /// Get all the games in the file
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        /// <param name="separator">The separator to split a line upon</param>
        /// <returns>The games</returns>
        /// <exception cref="InvalidDataException">If the file does not exists</exception>
        /// <exception cref="FileNotFoundException">If the file is empty or a line does not respect the agreed upon format</exception>
        public static List<Game> GetGames(string filePath, 
                                          char separator)
        {
            if (File.Exists(filePath))
            {
                var lines = File.ReadAllLines(filePath);

                if (lines.Length == 0)
                {
                    throw new InvalidDataException($"The file {filePath} is empty");
                }

                var games = new List<Game>();

                foreach (var line in lines)
                {
                    var gameInformations = line.Split(separator);

                    if (gameInformations.Length != 5)
                    {
                        throw new InvalidDataException($"The line : {line} in the file {filePath} does not respect the expected format <Title, Year, Category, Rating, Publisher>");
                    }

                    var game = Game.CreateGame(gameInformations[0],
                                               int.Parse(gameInformations[1]),
                                               int.Parse(gameInformations[2]),
                                               int.Parse(gameInformations[3]),
                                               int.Parse(gameInformations[4]));
                    games.Add(game);
                }

                return games;
            }

            throw new FileNotFoundException($"The file path {filePath} is invalid");
        }
    }
}
