﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    /// <summary>
    /// Interface to interact with the user
    /// </summary>
    public static class UserInterface
    {
        // Choix de menu
        public const int QUIT = 0;
        public const int ADD_GAME = 1;
        public const int MODIFY_GAME = 2;
        public const int DELETE_GAME = 3;
        public const int LIST_GAMES = 4;
        public const int ADD_PUBLISHER = 5;

        private static List<Publisher> publishersLibrary;
        private static List<Game> gamesLibrary;

        /// <summary>
        /// Run the user interface
        /// </summary>
        /// <param name="publishers">List of the available publishers</param>
        public static void Run(List<Publisher> publishers,
                               List<Game> games)
        {
            publishersLibrary = publishers;
            gamesLibrary = games;

            do
            {
                DisplayMenu();
                int userInput = ProcessUserInput(" Your choice: ", 0, 5, false);

                switch (userInput)
                {
                    case QUIT:
                        // quit
                        return;
                    case ADD_GAME:
                        AddGame();
                        break;
                    case MODIFY_GAME:
                        ModifyGame();
                        break;
                    case DELETE_GAME:
                        // Remove
                        break;
                    case LIST_GAMES:
                        ListGames();
                        break;
                    case ADD_PUBLISHER:
                        // Add publisher
                        break;

                } 
            } while (true);
        }

        private static void DisplayMenu()
        {
            Console.WriteLine("==========================");
            Console.WriteLine("***** Video Game App *****");
            Console.WriteLine("==========================");
            Console.WriteLine("");
            Console.WriteLine(" 0)	Quit");
            Console.WriteLine(" 1)	Add a game");
            Console.WriteLine(" 2)	Modify a game");
            Console.WriteLine(" 3)	Remove a game");
            Console.WriteLine(" 4)	List all games");
            Console.WriteLine(" 5)	Add a publisher");
            Console.WriteLine("");
        }

        private static void AddGame()
        {
            string title = string.Empty;
            do
            {
                if (title != string.Empty)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" Please enter a game title.");
                    Console.ResetColor();
                }

                Console.Write("Enter game's title: ");
                title = Console.ReadLine(); 
            } while (!Game.IsGameTitleValid(title));

            Console.WriteLine("Enter publishing's year between 1978 and 2024: ");
            int publishingYear = ProcessUserInput("Enter Year: ", 1978, 2024, false);

            Console.WriteLine(Game.GetAvailableCategoriesString());
            int category = ProcessUserInput("Enter category: ", 0, 4, false);

            int rating = ProcessUserInput("Enter rating: ", 0, int.MaxValue, false);

            Console.WriteLine(Publisher.GetAvailablePublishersString(publishersLibrary));
            int publisher = ProcessUserInput("Enter publisher: ", 0, publishersLibrary.Count - 1, false);

            var newGame = Game.CreateGame(title,
                                          publishingYear,
                                          category,
                                          rating,
                                          publisher);

            Library.InsertGame(gamesLibrary, newGame);
        }

        private static void ModifyGame()
        {
            Console.WriteLine(Library.GetLibraryString(gamesLibrary, publishersLibrary));

            int gameNumberToUpdate = ProcessUserInput("Enter game to update: (-1 to cancel)", -1, gamesLibrary.Count - 1, false);

            if(gameNumberToUpdate == -1)
            {
                return;
            }

            var gameToUpdate = gamesLibrary[gameNumberToUpdate];
            Console.Write($"Enter game's title (press Enter to leave {gameToUpdate.Title}): ");
            var newTitle = Console.ReadLine();

            if(newTitle == string.Empty)
            {
                newTitle = gameToUpdate.Title;
            }

            Console.WriteLine($"Enter publishing's year between 1978 and 2024 (press Enter to leave {gameToUpdate.Year}): ");
            int newPublishingYear = ProcessUserInput("Enter Year: ", 1978, 2024, true);

            if(newPublishingYear == -1)
            {
                newPublishingYear = gameToUpdate.Year;
            }

            Console.WriteLine(Game.GetAvailableCategoriesString());
            int newCategory = ProcessUserInput($"Enter category (press Enter to leave {gameToUpdate.GameCategory}): ", 0, 4, true);

            if(newCategory == -1)
            {
                newCategory = Game.GetCategoryFromName(gameToUpdate.GameCategory);
            }

            int newRating = ProcessUserInput($"Enter rating (press Enter to leave {gameToUpdate.Rating}): ", 0, int.MaxValue, true);

            if(newRating == -1)
            {
                newRating = gameToUpdate.Rating;
            }

            Console.WriteLine(Publisher.GetAvailablePublishersString(publishersLibrary));
            int newPublisher = ProcessUserInput($"Enter publisher (press Enter to leave {gameToUpdate.GamePublisher}): ", 0, publishersLibrary.Count - 1, true);

            if(newPublisher == -1)
            {
                newPublisher = gameToUpdate.GamePublisher;
            }

            var updatedGame = Game.CreateGame(newTitle,
                                              newPublishingYear,
                                              newCategory,
                                              newRating,
                                              newPublisher);

            Library.UpdateGame(gamesLibrary, gameNumberToUpdate, updatedGame);
        }

        private static void ListGames()
        {
            Console.WriteLine(Library.GetLibraryString(gamesLibrary, publishersLibrary));
        }

        private static int ProcessUserInput(string displayMessage, 
                                            int minimumValue,
                                            int maximumValue,
                                            bool canBeEmpty)
        {
            int userChoice = -1;
            bool inputIsOk;
            string userInput = string.Empty;

            do
            {
                // User input empty only on the firt do, otherwise it always has a value in int. 
                // If we do another run of the do while and the useInput variable is not empty then the user entered an incorrect value
                if (userInput != string.Empty)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" Incorrect value.");
                    Console.ResetColor();
                }

                Console.Write(displayMessage);
                userInput = Console.ReadLine();

                if(userInput == string.Empty && canBeEmpty)
                {
                    return -1; // user input can be empty when we update a game. We return a hardcoded -1 and not the user choice because we cannot be sur the
                               // user hasn't input something else invalide before entering enter
                }

                inputIsOk = int.TryParse(userInput, out userChoice);

            } while (!inputIsOk                  ||
                     userChoice < minimumValue   ||
                     userChoice > maximumValue);

            return userChoice;
        }
    }
}
